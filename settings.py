# -*- coding: utf-8 -*-

"""
Datetime: 2020/03/17
Author: Zhang Yafei
Description: 
"""
import sqlite3

import pymysql

DATABASE = {
    'sqlite3': {
        'creator': sqlite3,
        'maxcached': 50,
        'maxconnections': 1000,
        'maxusage': 1000,
        'database': 'sqlite3.db',
        'check_same_thread': False
    },
    'mysql': {
        'creator': pymysql,
        'mincached': 5,
        'host': ‘127.0.0.1',
        'user': 'root',
        'passwd': '0000',
        'db': 'COVID-19',
        'port': 3306,
    },
    'use_db': 'mysql',               # 'mysql
}
# DEBUG = True