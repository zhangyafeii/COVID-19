var right2_echart = echarts.init(document.getElementById('right2'));

var right2_option = {
        title: {
            text: '今日疫情热搜',
            x: 'left',
            textStyle:{
                color: "white",
            }
        },
        tooltip: {
            show: true
        },
        series: [{
            type: 'wordCloud',
            gridSize: 1,
            sizeRange: [12, 55],
            rotationRange: [-45, 0, 45, 90],
            textStyle: {
                normal: {
                    color: function() {
                        return 'rgb(' + [
                            Math.round(Math.random() * 255),
                            Math.round(Math.random() * 255),
                            Math.round(Math.random() * 255)
                        ].join(',') + ')';
                    }
                },
                emphasis: {
                    shadowBlur: 10,
                    shadowColor: '#333'
                }
            },
            right: null,
            bottom: null,
            data: [],
        }]
    };

right2_echart.setOption(right2_option);

function set_right2() {
    $.ajax({
        url: "/get_hot_search",
        type: "post",
        success: function (res) {
            right2_option.series[0].data = res;
            right2_echart.setOption(right2_option);
        }
    })
}

