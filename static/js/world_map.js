var world_echart = echarts.init(document.getElementById("mid2"), "dark");
var world_option = {
    tooltip: {
        trigger: 'item',
        formatter: function (params) {
            return params.name + '<br/>' + '确诊人数 : '
                + params.value + '<br/>' + '死亡人数 : '
                + params['data'].dead + '<br/>' + '治愈人数 : '
                + params['data'].heal
        }//数据格式化
    },
    title: {
        text:"全球疫情图",
        left: 'center',
    },
    legend: {
        orient: 'vertical',
        left: 'left',
        data: ['世界疫情图']
    },
     geo: {
        map: 'world',
        roam: false,//不开启缩放和平移
        // zoom: 1.1,//视角缩放比例
        label: {
            normal: {
                show: true,
                fontSize: '10',
                color: 'rgba(0,0,0,0.7)'
            }
        },
        itemStyle: {
            normal: {
                borderColor: 'rgba(0, 0, 0, 0.2)'
            },
            emphasis: {
                areaColor: '#F3B329',//鼠标选择区域颜色
                shadowOffsetX: 0,
                shadowOffsetY: 0,
                shadowBlur: 20,
                borderWidth: 0,
                shadowColor: 'rgba(0, 0, 0, 0.5)'
            }
        }
    },
    visualMap: {
        type: 'piecewise',
        pieces: [
            {min: 10000, max: 1000000, label: '>=10000人', color: '#4e160f'},
            {min: 1000, max: 9999, label: '确诊1000-9999人', color: '#974236'},
            {min: 100, max: 999, label: '确诊100-999人', color: '#CC2929'},
            {min: 10, max: 99, label: '确诊10-99人', color: '#ee7263'},
            {min: 1, max: 9, label: '确诊1-9人', color: '#f5bba7'},
        ],
        color: ['#E0022B', '#E09107', '#A3E00B'],
        textStyle: {
            color: "white"
        },
    },
    series: [{
        name: 'World Population (2010)',
        type: 'map',
        mapType: 'world',
        roam: true,
        itemStyle: {emphasis: {label: {show: true}}},
        data: []
    }]
};
world_echart.setOption(world_option);
