<div align=center>
	<img src="images/logo.jfif" width="600" height="300" />
</div>
<div align=center>
<img src="https://img.shields.io/badge/FLASK-1.0.2-lightBlue"/>
<img src="https://img.shields.io/badge/pymysql-0.9.1-brightgreen"/>
<img src="https://img.shields.io/badge/requests-2.23.0-green"/>
<img src="https://img.shields.io/badge/jieba-0.39-gold"/>                       
<img src="https://img.shields.io/badge/selenium-3.14.0-blue"/>
<img src="https://img.shields.io/badge/DBUtils-1.3-red"/>
</div>

## 基本介绍

​        基于 requests 模块获取腾讯提供的疫情实时数据，包含国内最新、国内历史，全球最新、全球历 史数据，以及利用 selenium 模块获取最新百度热搜榜关键词，并将获取的结构化数据存储到 MySQL 中。 通过 Flask 搭建 Web 项目，并使用 Echarts 进行数据的可视化展示，利用 ngnix 和 gunicorn 进行项目部署， 基于 crontab 的定时任务实现数据的定时爬取，确保数据的实时自动更新。

- 技术列表：Python 网络爬虫/MySQL/Flask/Echarts/nginx/ gunicorn/crontab 定时任务
- 在线预览：http://182.254.179.186/

## 数据准备

```
第一步：pip install -r requirements.txt
第二步：根据sql文件夹下的sql建表语句新建数据表
```
## 修改数据库配置

> ```python
> DATABASE = {
>     'sqlite3': {
>         'creator': sqlite3,
>         'maxcached': 50,
>         'maxconnections': 1000,
>         'maxusage': 1000,
>         'database': 'sqlite3.db',
>         'check_same_thread': False
>     },
>     'mysql': {
>         'creator': pymysql,
>         'mincached': 5,
>         'host': '127.0.0.1',
>         'user': 'root',
>         'passwd': '0000',
>         'db': 'COVID-19',
>         'port': 3306,
>     }
> }
> ```

## 更新疫情数据

```python
python spider.py
'''
G:\programming\中大项目\COVID-19>python spider.py
2020-03-20 12:04:15.035278 获取数据完成 正在更新数据库
china history 已经是最新数据 暂时无需更新
world history 已经是最新数据 暂时无需更新
world lasted 更新完毕
china lasted 更新完毕
2020-03-20 12:05:30.468485 所有数据更新完毕

DevTools listening on ws://127.0.0.1:55447/devtools/browser/2fc040f4-08ee-4a46-b2ed-4ba1b4eccb43
[0320/120534.833:ERROR:mf_helpers.cc(14)] Error in dxva_video_decode_accelerator_win.cc on line 511
2020-03-20 12:05:37     数据更新完毕
'''
```

## 本地运行

![](images/运行.png)

![](images/本地运行页面.png)

![](images/本地全球疫情.png)

## 项目部署

- 第一步：将项目文件拷贝到服务器
- 第二步：安装nginx和gunicorn

```
yun install nginx
pip install gunicorn
```

- 第三步：启动gunicorn

  ```
  gunicorn -b 127.0.0.1:8080 -D manage:app
  # 重启
  pstree -ap|grep gunicorn
    |-gunicorn,18598 /usr/local/python3/bin/gunicorn -b 127.0.0.1:8080 -D manage:app
    |   `-gunicorn,18601 /usr/local/python3/bin/gunicorn -b 127.0.0.1:8080 -D manage:app
    |           |-grep,18822 --color=auto gunicorn
  kill -HUP 
  ```

- 第四步：编辑nginx配置文件：

  vim /etc/nginx/conf.d/default.conf

  ![](images/nginx_conf.png)

  - 第五步：启动nginx

    ```
    /usr/sbin/nginx
    若已经启动： ps -A | grep nginx
    kill -9 pid1
    kill -9 pid2
    ```


  - 第六步：访问ip地址

 ![](images/全国疫情实时追踪.png)

![](images/全球疫情实时追踪.png)

## 部署定时爬虫

```shell
yum install https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm
下载指定版本解压之后上传到项目更目录 https://npm.taobao.org/mirrors/chromedriver/
赋予chromedirver权限  chmod +x chromedriver
crontab -e  编辑 添加
0 7-22/1 * * * python /root/web/covid-19/spider.py >> /root/web/log_spider 2>&1 &
查看日志
cat log_spider
```

- crontab定时任务 

  <https://www.cnblogs.com/aminxu/p/5993769.html>

