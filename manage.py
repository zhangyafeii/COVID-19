# -*- coding: utf-8 -*-

"""
Datetime: 2020/03/18
Author: Zhang Yafei
Description: 
"""
from flask import Flask, render_template, jsonify
import utils

app = Flask(__name__)
# 设置配置文件
app.config.from_pyfile('settings.py')


@app.route('/')
def index():
    return render_template('main.html')


@app.route('/get_china_lasted_num', methods=['GET','POST'])
def get_china_lasted_num():
    res = utils.get_china_history_data()
    response = {'confirm': int(res[0]), 'suspect': int(res[1]), 'heal': int(res[2]), 'dead': int(res[3])}
    return jsonify(response)


@app.route("/get_china_map_data", methods=['GET',"POST"])
def get_china_map_data():
    response = {'code': 200, 'data': []}
    try:
        response['data'] = utils.get_china_province_lasted_data()
        return jsonify(response)
    except Exception as e:
        response['code'] = 201
        response['data'] = str(e)
        return jsonify(response)


@app.route('/get_china_total_trend', methods=['GET', 'POST'])
def get_china_total_trend():
    days, confirm, suspect, dead, heal = utils.get_china_total_data()
    return jsonify({'days': days, 'confirm': confirm, 'suspect': suspect, 'dead': dead, 'heal': heal})


@app.route('/get_china_add_trend', methods=['GET', 'POST'])
def get_china_add_trend():
    days, confirm, suspect, dead, heal = utils.get_china_add_data()
    return jsonify({'days': days, 'confirm': confirm, 'dead': dead, 'heal': heal})


@app.route('/get_china_top5', methods=['GET', 'POST'])
def get_china_top5():
    city, confirm = utils.get_nohubei_top5()
    return jsonify({'city': city, 'confirm': confirm})


@app.route('/get_world_lasted_num', methods=['GET','POST'])
def get_world_lasted_num():
    res = utils.get_world_lasted_data()
    response = {'confirm': int(res[0]), 'confirm_now': int(res[1]), 'heal': int(res[2]), 'dead': int(res[3])}
    return jsonify(response)


@app.route('/get_world_total_trend', methods=['GET', 'POST'])
def get_world_total_trend():
    days, confirm, dead, heal = utils.get_world_total_data()
    return jsonify({'days': days, 'confirm': confirm, 'dead': dead, 'heal': heal})


@app.route('/get_world_add_trend', methods=['GET', 'POST'])
def get_world_add_trend():
    days, confirm_add = utils.get_world_add_data()
    return jsonify({'days': days, 'confirm': confirm_add})


@app.route('/get_world_top5', methods=['GET', 'POST'])
def get_world_top5():
    country, confirm = utils.get_world_top5()
    return jsonify({'country': country, 'confirm': confirm})


@app.route("/get_world_map_data", methods=['GET',"POST"])
def get_world_map_data():
    response = {'code': 200, 'data': []}
    try:
        response['data'] = utils.get_world_country_lasted_data()
        return jsonify(response)
    except Exception as e:
        print(e)
        response['code'] = 201
        response['data'] = str(e)
        return jsonify(response)


@app.route('/get_hot_search', methods=['GET', 'POST'])
def get_hot_search():
    data = utils.get_hotsearch()
    return jsonify(data)


@app.route('/world', methods=['GET','POST'])
def world():
    return render_template('world.html')


if __name__ == '__main__':
    app.run(debug=True)

