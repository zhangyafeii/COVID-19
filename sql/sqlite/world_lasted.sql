/*
 Navicat Premium Data Transfer

 Source Server         : sqlite3
 Source Server Type    : SQLite
 Source Server Version : 3030001
 Source Schema         : main

 Target Server Type    : SQLite
 Target Server Version : 3030001
 File Encoding         : 65001

 Date: 22/03/2020 11:18:54
*/

PRAGMA foreign_keys = false;

-- ----------------------------
-- Table structure for world_lasted
-- ----------------------------
DROP TABLE IF EXISTS "world_lasted";
CREATE TABLE "world_lasted" (
  "date" date,
  "country" TEXT NOT NULL,
  "confirm" integer,
  "confirm_now" integer,
  "suspect" integer,
  "dead" integer,
  "heal" integer,
  "confirm_add" integer,
  "dead_add" integer,
  "heal_add" integer,
  "dead_rate" TEXT,
  "heal_rate" TEXT,
  "country_english_name" TEXT,
  PRIMARY KEY ("country")
);

PRAGMA foreign_keys = true;
