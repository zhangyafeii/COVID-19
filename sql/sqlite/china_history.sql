/*
 Navicat Premium Data Transfer

 Source Server         : sqlite3
 Source Server Type    : SQLite
 Source Server Version : 3030001
 Source Schema         : main

 Target Server Type    : SQLite
 Target Server Version : 3030001
 File Encoding         : 65001

 Date: 20/03/2020 12:12:35
*/

PRAGMA foreign_keys = false;

-- ----------------------------
-- Table structure for china_history
-- ----------------------------
DROP TABLE IF EXISTS "china_history";
CREATE TABLE "china_history" (
  "date" DATE NOT NULL COLLATE NOCASE,
  "confirm" integer,
  "overseas_inputs" integer,
  "suspect" integer,
  "dead" integer,
  "heal" integer,
  "dead_rate" integer,
  "heal_rate" integer,
  "confirm_add" integer,
  "dead_add" integer,
  "suspect_add" integer,
  "heal_add" integer,
  "overseas_inputs_add" integer,
  PRIMARY KEY ("date")
);

PRAGMA foreign_keys = true;
