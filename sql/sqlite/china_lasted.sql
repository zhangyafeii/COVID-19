/*
 Navicat Premium Data Transfer

 Source Server         : sqlite3
 Source Server Type    : SQLite
 Source Server Version : 3030001
 Source Schema         : main

 Target Server Type    : SQLite
 Target Server Version : 3030001
 File Encoding         : 65001

 Date: 20/03/2020 12:12:42
*/

PRAGMA foreign_keys = false;

-- ----------------------------
-- Table structure for china_lasted
-- ----------------------------
DROP TABLE IF EXISTS "china_lasted";
CREATE TABLE "china_lasted" (
  "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  "update_time" datetime,
  "country" TEXT,
  "province" TEXT,
  "city" TEXT,
  "confirm" integer,
  "suspect" integer,
  "dead" integer,
  "heal" integer,
  "confirm_add" integer,
  "dead_rate" text,
  "heal_rate" text,
  CONSTRAINT "city_unique" UNIQUE ("country" ASC, "province" ASC, "city" ASC) ON CONFLICT REPLACE
);

-- ----------------------------
-- Auto increment value for china_lasted
-- ----------------------------
UPDATE "sqlite_sequence" SET seq = 1746 WHERE name = 'china_lasted';

PRAGMA foreign_keys = true;
