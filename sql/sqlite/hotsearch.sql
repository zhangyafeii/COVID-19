/*
 Navicat Premium Data Transfer

 Source Server         : sqlite3
 Source Server Type    : SQLite
 Source Server Version : 3030001
 Source Schema         : main

 Target Server Type    : SQLite
 Target Server Version : 3030001
 File Encoding         : 65001

 Date: 20/03/2020 12:12:48
*/

PRAGMA foreign_keys = false;

-- ----------------------------
-- Table structure for hotsearch
-- ----------------------------
DROP TABLE IF EXISTS "hotsearch";
CREATE TABLE "hotsearch" (
  "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  "update_time" DATETIME,
  "content" TEXT
);

-- ----------------------------
-- Auto increment value for hotsearch
-- ----------------------------
UPDATE "sqlite_sequence" SET seq = 100 WHERE name = 'hotsearch';

PRAGMA foreign_keys = true;
