/*
 Navicat Premium Data Transfer

 Source Server         : sqlite3
 Source Server Type    : SQLite
 Source Server Version : 3030001
 Source Schema         : main

 Target Server Type    : SQLite
 Target Server Version : 3030001
 File Encoding         : 65001

 Date: 02/04/2020 12:40:04
*/

PRAGMA foreign_keys = false;

-- ----------------------------
-- Table structure for world_statis
-- ----------------------------
DROP TABLE IF EXISTS "world_statis";
CREATE TABLE "world_statis" (
  "now_confirm" integer,
  "confirm" integer,
  "heal" integer,
  "dead" integer,
  "now_confirm_add" integer,
  "confirm_add" integer,
  "heal_add" integer,
  "dead_add" integer,
  "update_time" datetime
);

PRAGMA foreign_keys = true;
