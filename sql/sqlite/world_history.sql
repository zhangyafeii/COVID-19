/*
 Navicat Premium Data Transfer

 Source Server         : sqlite3
 Source Server Type    : SQLite
 Source Server Version : 3030001
 Source Schema         : main

 Target Server Type    : SQLite
 Target Server Version : 3030001
 File Encoding         : 65001

 Date: 20/03/2020 12:12:55
*/

PRAGMA foreign_keys = false;

-- ----------------------------
-- Table structure for world_history
-- ----------------------------
DROP TABLE IF EXISTS "world_history";
CREATE TABLE "world_history" (
  "date" date NOT NULL,
  "confirm" integer,
  "dead" integer,
  "heal" integer,
  "confirm_add" integer,
  "dead_rate" TEXT,
  "heal_rate" TEXT,
  PRIMARY KEY ("date")
);

PRAGMA foreign_keys = true;
