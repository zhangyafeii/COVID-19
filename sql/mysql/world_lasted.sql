/*
 Navicat Premium Data Transfer

 Source Server         : centos@mysql
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : 182.254.179.186:3306
 Source Schema         : COVID-19

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 22/03/2020 11:18:13
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for world_lasted
-- ----------------------------
DROP TABLE IF EXISTS `world_lasted`;
CREATE TABLE `world_lasted`  (
  `date` datetime(0) NULL DEFAULT NULL,
  `country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `confirm` int(255) NULL DEFAULT NULL,
  `confirm_now` int(255) NULL DEFAULT NULL,
  `suspect` int(255) NULL DEFAULT NULL,
  `dead` int(255) NULL DEFAULT NULL,
  `heal` int(255) NULL DEFAULT NULL,
  `confirm_add` int(255) NULL DEFAULT NULL,
  `dead_add` int(255) NULL DEFAULT NULL,
  `heal_add` int(255) NULL DEFAULT NULL,
  `dead_rate` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `heal_rate` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `country_english_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`country`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
