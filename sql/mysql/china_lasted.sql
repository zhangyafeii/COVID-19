/*
 Navicat Premium Data Transfer

 Source Server         : centos@mysql
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : 182.254.179.186:3306
 Source Schema         : COVID-19

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 20/03/2020 12:09:10
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for china_lasted
-- ----------------------------
DROP TABLE IF EXISTS `china_lasted`;
CREATE TABLE `china_lasted`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `province` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `confirm` int(255) NULL DEFAULT NULL,
  `suspect` int(255) NULL DEFAULT NULL,
  `dead` int(255) NULL DEFAULT NULL,
  `heal` int(255) NULL DEFAULT NULL,
  `confirm_add` int(255) NULL DEFAULT NULL,
  `dead_rate` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `heal_rate` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `no_account`(`country`, `province`, `city`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 441 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
