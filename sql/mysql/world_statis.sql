/*
 Navicat Premium Data Transfer

 Source Server         : centos@mysql
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : 182.254.179.186:3306
 Source Schema         : COVID-19

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 02/04/2020 12:38:46
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for world_statis
-- ----------------------------
DROP TABLE IF EXISTS `world_statis`;
CREATE TABLE `world_statis`  (
  `now_confirm` int(255) NULL DEFAULT NULL,
  `confirm` int(255) NULL DEFAULT NULL,
  `heal` int(255) NULL DEFAULT NULL,
  `dead` int(255) NULL DEFAULT NULL,
  `now_confirm_add` int(255) NULL DEFAULT NULL,
  `confirm_add` int(255) NULL DEFAULT NULL,
  `heal_add` int(255) NULL DEFAULT NULL,
  `dead_add` int(255) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
